#include <LDateTime.h>

#include <GPS_GPRMC.h>

#include <LGPS.h>

gpsSentenceInfoStruct info;
unsigned int timestamp;
datetimeInfo date;

void setup() {

  delay(5000);

  Serial.begin(9600);
  LGPS.powerOn();

  Serial.println("GPS on");

  String dateTime;

  while (true) {

    LGPS.getData(&info);
    GPS_GPRMC dataObj((String)(char*)info.GPRMC);

    if (dataObj.validity == "A") {

      dateTime = dataObj.date + dataObj.time;

      break;

    } else {
      Serial.println((String)(char*)info.GPRMC);
      delay(1000);
    }

  }

  String year = "20" + dateTime.substring(4, 6);

  date.year = year.toInt();
  date.mon = dateTime.substring(2, 4).toInt();
  date.day = dateTime.substring(0, 2).toInt();
  date.hour = dateTime.substring(6, 8).toInt();
  date.min = dateTime.substring(8, 10).toInt();
  date.sec = dateTime.substring(10, 12).toInt();

  Serial.println((String) date.year + " " + date.mon + " " + date.day);

  LDateTime.setTime(&date);

}

void loop() {
  LDateTime.getTime(&date);

  Serial.println((String) date.year + " " +
                 date.mon + " " + date.day + " " +
                 date.hour + " " + date.min + " " + date.sec);

  delay(4000);
}
