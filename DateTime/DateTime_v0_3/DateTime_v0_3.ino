
#include <LDateTime.h>

#include <GPS_GPRMC.h>

#include <LGPS.h>

void setup() {

  delay(5000);

  Serial.begin(9600);

  bool set = SystemTime.setSystemTimeByGPS(60);

  if (set) {
    Serial.println("Set time");
  } else {
    Serial.println("Didn't set time");
  }

}

void loop() {
  Serial.println(SystemTime.getSystemTime());
  delay(1000);
}


