#include <LDateTime.h>

#include <GPS_GPRMC.h>

#include <LGPS.h>

void setup() {

  delay(5000);

  Serial.begin(9600);

  bool set = setSystemTimeByGPS(60);

  if (set) {
    Serial.println("Set time");
  } else {
    Serial.println("Didn't set time");
  }

}

void loop() {
  Serial.println(getSystemTime());
  delay(1000);
}

String getSystemTime() {

  datetimeInfo date;

  LDateTime.getTime(&date);

  String mon = (String) date.mon;

  if (date.mon < 10) {
    mon = "0" + mon;
  }

  String day = (String) date.day;

  if (date.day < 10) {
    day = "0" + day;
  }

  String hour = (String) date.hour;

  if (date.hour < 10) {
    hour = "0" + hour;
  }

  String min = (String) date.min;

  if (date.min < 10) {
    min = "0" + min;
  }

  String sec = (String) date.sec;

  if (date.sec < 10) {
    sec = "0" + sec;
  }

  return ((String) date.year + "/" +
          mon + "/" + day + " " +
          hour + ":" + min + ":" + sec);

}

boolean setSystemTimeByGPS(int timeOutSeconds) {

  int maxTimestamp = millis() + (30 * 1000);
  gpsSentenceInfoStruct info;
  datetimeInfo date;

  LGPS.powerOn();

  while (millis() < maxTimestamp) {

    LGPS.getData(&info);
    GPS_GPRMC dataObj((String)(char*)info.GPRMC);

    if (dataObj.validity == "A") {

      LGPS.powerOff();

      String dateTime = dataObj.date + dataObj.time;

      String year = "20" + dateTime.substring(4, 6);
      date.year = year.toInt();
      date.mon = dateTime.substring(2, 4).toInt();
      date.day = dateTime.substring(0, 2).toInt();
      date.hour = dateTime.substring(6, 8).toInt();
      date.min = dateTime.substring(8, 10).toInt();
      date.sec = dateTime.substring(10, 12).toInt();

      LDateTime.setTime(&date);

      return true;

    } else {
      delay(100);
    }

  }

  return false;

}
