#include <LGPS.h>
#include <LDateTime.h>
#include <GPS_GPRMC.h>

void setup() {

  delay(5000);

  Serial.begin(9600);

  Serial.println("Getting time from GPS");

  setSystemTimeByGPS(60);

  Serial.println("Got time from GPS:");

  Serial.println(getSystemTime());

  Serial.println(getSystemDateTimeInFormat("y-m-d H:i:s"));

}

void loop() {

}

String getSystemTime() {

  datetimeInfo date;

  LDateTime.getTime(&date);

  String mon = (String) date.mon;

  if (date.mon < 10) {
    mon = "0" + mon;
  }

  String day = (String) date.day;

  if (date.day < 10) {
    day = "0" + day;
  }

  String hour = (String) date.hour;

  if (date.hour < 10) {
    hour = "0" + hour;
  }

  String min = (String) date.min;

  if (date.min < 10) {
    min = "0" + min;
  }

  String sec = (String) date.sec;

  if (date.sec < 10) {
    sec = "0" + sec;
  }

  return ((String) date.year + "/" +
          mon + "/" + day + " " +
          hour + ":" + min + ":" + sec);

}

boolean setSystemTimeByGPS(int timeOutSeconds) {

  int maxTimestamp = millis() + (timeOutSeconds * 1000);
  gpsSentenceInfoStruct info;
  datetimeInfo date;

  LGPS.powerOn();

  while (millis() < maxTimestamp) {

    LGPS.getData(&info);
    GPS_GPRMC dataObj((String)(char*)info.GPRMC);

    if (dataObj.validity == "A") {

      LGPS.powerOff();

      String dateTime = dataObj.date + dataObj.time;

      String year = "20" + dateTime.substring(4, 6);
      date.year = year.toInt();
      date.mon = dateTime.substring(2, 4).toInt();
      date.day = dateTime.substring(0, 2).toInt();
      date.hour = dateTime.substring(6, 8).toInt();
      date.min = dateTime.substring(8, 10).toInt();
      date.sec = dateTime.substring(10, 12).toInt();

      LDateTime.setTime(&date);

      return true;

    } else {
      delay(100);
    }

  }

  return false;

}

String getSystemDateTimeInFormat(String format) {

  datetimeInfo date;

  LDateTime.getTime(&date);

  String ret;

  char lastChar = 0;

  for (int i = 0; i < format.length(); i++) {
    if (lastChar != '\\') {
      switch (format[i]) {
        case 'Y':
          {
            String year = (String) date.year;
            if (date.year < 10) {
              year = "0" + year;
            }
            ret += year;
            break;
          }
        case 'y':
          ret += ((String) date.year).substring(2);
          break;
        case 'm':
          {
            String mon = (String) date.mon;
            if (date.mon < 10) {
              mon = "0" + mon;
            }
            ret += mon;
            break;
          }
        case 'd':
          {
            String day = (String) date.day;
            if (date.day < 10) {
              day = "0" + day;
            }
            ret += day;
            break;
          }
        case 'H':
          {
            String hour = (String) date.hour;
            if (date.hour < 10) {
              hour = "0" + hour;
            }
            ret += hour;
            break;
          }
        case 'i':
          {
            String min = (String) date.min;
            if (date.min < 10) {
              min = "0" + min;
            }
            ret += min;
            break;
          }
        case 's':
          {
            String sec = (String) date.sec;
            if (date.sec < 10) {
              sec = "0" + sec;
            }
            ret += sec;
            break;
          }
        default:
          ret += (String) format[i];
          break;
      }
    } else {
      ret += (String) format[i];
    }
    lastChar = format[i];
  }
  return ret;
}

