#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>
#include <LTask.h>
#include <LGSM.h>

LSDClass sd;
char* fileName = (char*) "sms_2014_09_23.txt";
LFile smsFile;


void setup()
{
  Serial.begin(9600);

  while (!LSMS.ready())
  {
    delay(1000);
  }

  sd.begin();
  smsFile = sd.open(fileName, FILE_WRITE);

}


void loop()
{

  char p_num[20];
  int len = 0;
  char dtaget[160];

  if (LSMS.available()) // Check if there is new SMS
  {

    LSMS.remoteNumber(p_num, 20); // display Number part

    smsFile.write("Message received from ");
    smsFile.write(p_num);
    smsFile.write(": "); // display Content part

    while (true)
    {
      int v = LSMS.read();
      if (v < 0)
        break;

      dtaget[len++] = (char)v;
      smsFile.write((char)v);
    }

    smsFile.write("\n");
    LSMS.flush(); // delete message
  }
}
