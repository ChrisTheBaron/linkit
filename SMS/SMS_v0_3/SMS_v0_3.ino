#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>
#include <LTask.h>
#include <LGSM.h>
#include <LGPS.h>
#include <LDateTime.h>
#include <GPS_GPRMC.h>

LSDClass sd;
char* fileName = (char*) "whitelist.txt";
LFile smsFile;

void setup() {

  delay(5000);

  Serial.begin(9600);

  Serial.println("Getting time from GPS");

  setSystemTimeByGPS(60);

  Serial.println("Got time from GPS:");

  Serial.println(getSystemTime());

  while (!LSMS.ready())
  {
    delay(1000);
  }

  Serial.println("SMS ready to receive");

  sd.begin();

}

void loop() {

  char p_num[20];
  int len = 0;
  char dtaget[160];

  if (LSMS.available()) // Check if there is new SMS
  {

    Serial.println("Text Recieved");

    String timeString = getSystemTime();

    char timeArray[timeString.length()];

    timeString.toCharArray(timeArray, timeString.length() + 1);

    Serial.print("At time: ");

    Serial.println(timeString);

    LSMS.remoteNumber(p_num, 20); // display Number part

    Serial.print("From: ");

    Serial.println(p_num);

    smsFile = sd.open(fileName, FILE_WRITE);

    smsFile.write(timeArray);
    smsFile.write(" - Message received from ");
    smsFile.write(p_num);
    smsFile.write(": "); // display Content part

    Serial.println("Writing to file");

    while (true)
    {
      int v = LSMS.read();
      if (v < 0)
        break;

      dtaget[len++] = (char)v;
      smsFile.write((char)v);
    }

    Serial.println("Written to file");

    smsFile.write("\n");
    smsFile.close();

    LSMS.flush(); // delete message


  }

}

String getSystemTime() {

  datetimeInfo date;

  LDateTime.getTime(&date);

  String mon = (String) date.mon;

  if (date.mon < 10) {
    mon = "0" + mon;
  }

  String day = (String) date.day;

  if (date.day < 10) {
    day = "0" + day;
  }

  String hour = (String) date.hour;

  if (date.hour < 10) {
    hour = "0" + hour;
  }

  String min = (String) date.min;

  if (date.min < 10) {
    min = "0" + min;
  }

  String sec = (String) date.sec;

  if (date.sec < 10) {
    sec = "0" + sec;
  }

  return ((String) date.year + "/" +
          mon + "/" + day + " " +
          hour + ":" + min + ":" + sec);

}

boolean setSystemTimeByGPS(int timeOutSeconds) {

  int maxTimestamp = millis() + (30 * 1000);
  gpsSentenceInfoStruct info;
  datetimeInfo date;

  LGPS.powerOn();

  while (millis() < maxTimestamp) {

    LGPS.getData(&info);
    GPS_GPRMC dataObj((String)(char*)info.GPRMC);

    if (dataObj.validity == "A") {

      LGPS.powerOff();

      String dateTime = dataObj.date + dataObj.time;

      String year = "20" + dateTime.substring(4, 6);
      date.year = year.toInt();
      date.mon = dateTime.substring(2, 4).toInt();
      date.day = dateTime.substring(0, 2).toInt();
      date.hour = dateTime.substring(6, 8).toInt();
      date.min = dateTime.substring(8, 10).toInt();
      date.sec = dateTime.substring(10, 12).toInt();

      LDateTime.setTime(&date);

      return true;

    } else {
      delay(100);
    }

  }

  return false;

}
