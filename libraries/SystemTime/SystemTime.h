#ifndef SystemTime_h
#define SystemTime_h

#include "Arduino.h"

class SystemTime {
   
  public:
	SystemTime();
	String getSystemTime();
	boolean setSystemTimeByGPS(int timeOutSeconds);
};


#endif