#include "Arduino.h"
#include "GPS_GPGGA.h"

GPS_GPGGA::GPS_GPGGA(String _rawData) {
      
      dataSeparator = ",";
      
      rawData = _rawData;

      int indexOfSeparator = _rawData.indexOf(dataSeparator);
	  
      gpsFormat = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);
	  
      time = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      latitude = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      latitude += _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      longitude = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      latitude += _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      gpsFixQuality = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      numberOfSatellites = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      hdop = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      altitude = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      altitude += _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      geoidalSeperation = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      geoidalSeperation += _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      timeSinceUpdate = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      referenceStationID = _rawData.substring(0, indexOfSeparator);
	  
    };
