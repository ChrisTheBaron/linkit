#ifndef GPS_GPGGA_h
#define GPS_GPGGA_h

#include "Arduino.h" 
 
class GPS_GPGGA {

  protected:
    String rawData;
    String dataSeparator;

  public:

    String gpsFormat;
    String time;
    String latitude;
    String longitude;
    String gpsFixQuality;
    String numberOfSatellites;
    String hdop;
    String altitude;
    String geoidalSeperation;
    String timeSinceUpdate;
    String referenceStationID;

    GPS_GPGGA(String _rawData);

};

#endif