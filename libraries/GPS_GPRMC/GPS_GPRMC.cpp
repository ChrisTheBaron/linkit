#include "Arduino.h"
#include "GPS_GPRMC.h"

GPS_GPRMC::GPS_GPRMC(String _rawData) {
      
      dataSeparator = ",";
      
      rawData = _rawData;

      int indexOfSeparator = _rawData.indexOf(dataSeparator);
	  
      gpsFormat = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);
	  
      time = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

	  indexOfSeparator = _rawData.indexOf(dataSeparator);
	  
      validity = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);

      latitude = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      latitude += _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      longitude = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);

      indexOfSeparator = _rawData.indexOf(dataSeparator);

      longitude += _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);

      speed = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);

      trueCourse = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);

      date = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);

      variation = _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
      indexOfSeparator = _rawData.indexOf(dataSeparator);

      variation += _rawData.substring(0, indexOfSeparator);
	  
      _rawData = _rawData.substring(indexOfSeparator + 1);
	  
    };