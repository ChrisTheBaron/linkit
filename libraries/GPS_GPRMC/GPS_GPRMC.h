#ifndef GPS_GPRMC_h
#define GPS_GPRMC_h

#include "Arduino.h" 
 
class GPS_GPRMC {

  protected:
    String rawData;
    String dataSeparator;

  public:

    String gpsFormat;
    String time;
    String validity;
    String latitude;
    String longitude;
    String speed;
    String trueCourse;
    String date;
    String variation;
    
    GPS_GPRMC(String _rawData);

};

#endif