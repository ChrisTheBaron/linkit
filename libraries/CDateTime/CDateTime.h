#ifndef CDateTime_h
#define CDateTime_h

#include "Arduino.h"

class CDateTime {

  protected:
	datetimeInfo dtiObj;
  
  public:
	CDateTime();
    CDateTime(int _year, int _month , int _day , int _hour , int _min , int _sec);
	void setAsSytemTime();
	
	
};


#endif