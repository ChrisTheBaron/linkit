#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>

LSDClass sd;
char* fileName = (char*) "whitelist.txt";


void setup() {
  // put your setup code here, to run once:

  delay(10000);//to let me get the console up
  Serial.begin(9600);
  Serial.println("Start of script");

  if (sd.begin()) {
    Serial.println("SD started correctly");
  } else {
    Serial.println("SD didn't start correctly");
  }

  if (sd.exists(fileName)) {
    Serial.println("File exists");
  } else {
    Serial.println("File does not exist");
  }

  LFile logFile = sd.open(fileName, FILE_WRITE);

  if (logFile) {
    Serial.println("File was opened");

    if (logFile.write("Adding log entry")) {
      Serial.println("Wrote to file");

      logFile.close();

      Serial.println("Closed file");

    } else {
      Serial.println("Couldn't write to file");
    }

  } else {
    Serial.println("File wasn't opened");
  }
}



void loop() {
  // put your main code here, to run repeatedly:

}
