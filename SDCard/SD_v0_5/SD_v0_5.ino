#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>

void setup() {
  // put your setup code here, to run once:

  delay(6000);//to let me get the console up
  Serial.begin(9600);

  LSDClass sd;

  if (sd.begin()) {

    char* fileName = (char*) "whitelist.csv";

    if (sd.exists(fileName)) {
      Serial.println("File exists");
    } else {
      Serial.println("File does not exist");
    }

    LFile logFile = sd.open(fileName, FILE_READ);

    if (logFile) {
      Serial.println("File was opened");

      char currentChar;
      char fileDelimiter = ',';
      String fileContent = "";
      int fileEntries = 1;

      for (int i = logFile.position (); i < logFile.size(); i++) {

        currentChar = logFile.read();

        fileContent += currentChar;

        if (currentChar == fileDelimiter) {
          fileEntries++;
        }

      }

      String entryArray[fileEntries];

      String tempStr = "";
      int currentEntry = 0;

      for (int i = 0; i < logFile.size(); i++) {

        if (fileContent[i] == fileDelimiter) {
          entryArray[currentEntry] = tempStr;
          tempStr = "";
          currentEntry ++;
        } else {
          tempStr += (String) fileContent[i];
        }

      }

      entryArray[currentEntry] = tempStr;

      for (int i = 0; i < fileEntries; i++) {
        Serial.println(entryArray[i]);
      }
      
      logFile.close();

      Serial.println("Done");

    } else {
      Serial.println("File wasn't opened");
    }

  } else {
    Serial.println("SD didn't start correctly");
  }

}



void loop() {
  // put your main code here, to run repeatedly:
}
