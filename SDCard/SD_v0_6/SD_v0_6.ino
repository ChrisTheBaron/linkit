#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>
#include <LTask.h>
#include <LGSM.h>
#include <LGPS.h>
#include <LDateTime.h>
#include <GPS_GPRMC.h>

LSDClass sd;
LFile smsFile;

String* whitelistArray;
int numberOfEntries;

void setup() {
  // put your setup code here, to run once:

  delay(6000);//to let me get the console up
  Serial.begin(9600);

  sd.begin();

  numberOfEntries = populateWhitelist((char*) "whitelist.csv",whitelistArray);
}



void loop() {
  delay(5000);


  Serial.println("Entries:");

  for (int i = 0; i < numberOfEntries; i++) {
    Serial.println(whitelistArray[i]);
  }


}

int populateWhitelist(char* fileName, String* &entryArray) {

  /**
   *  Opens whitelist and holds in memory
  **/
  LFile whitelistFile = sd.open(fileName, FILE_READ);
  char currentChar;
  char fileDelimiter = ',';
  String fileContent = "";
  int fileEntries = 1;
  for (int i = whitelistFile.position (); i < whitelistFile.size(); i++) {
    currentChar = whitelistFile.read();
    fileContent += currentChar;
    if (currentChar == fileDelimiter) {
      fileEntries++;
    }
  }

  entryArray = new String[fileEntries];

  String tempStr = "";
  int currentEntry = 0;
  for (int i = 0; i < whitelistFile.size(); i++) {
    if (fileContent[i] == fileDelimiter) {
      entryArray[currentEntry] = tempStr;
      tempStr = "";
      currentEntry ++;
    } else {
      tempStr += (String) fileContent[i];
    }
  }
  entryArray[currentEntry] = tempStr;
  whitelistFile.close();

  return fileEntries;

}


