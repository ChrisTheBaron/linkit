#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>

LSDClass sd;
char* fileName = (char*) "whitelist.csv";


void setup() {
  // put your setup code here, to run once:

  delay(10000);//to let me get the console up
  Serial.begin(9600);
  Serial.println("Start of script");

  if (sd.begin()) {
    Serial.println("SD started correctly");
  } else {
    Serial.println("SD didn't start correctly");
  }

  if (sd.exists(fileName)) {
    Serial.println("File exists");
  } else {
    Serial.println("File does not exist");
  }

  LFile logFile = sd.open(fileName, FILE_READ);

  if (logFile) {
    Serial.println("File was opened");

    Serial.print("Size of file: ");
    Serial.println(logFile.size());

    Serial.print("Data available: ");
    Serial.println(logFile.available());

    Serial.print("Cursor position: ");
    Serial.println(logFile.position());

    for (int i = logFile.position (); i < logFile.size(); i++) {
      Serial.write(logFile.read());
    }

    Serial.print("\n\n\n\n");

    Serial.println("End of file reached");

    logFile.close();

  } else {
    Serial.println("File wasn't opened");
  }
}



void loop() {
  // put your main code here, to run repeatedly:

}
