#include <LGPS.h>

gpsSentenceInfoStruct info;

void setup() {

  Serial.begin(9600);

  delay(10000);

  Serial.println("Before turning on GPS");

  LGPS.powerOn();

  Serial.println("After turning on GPS");

  delay(500);
}

void loop() {
  LGPS.getData(&info);
  Serial.println((char *)info.GPGGA);
  delay(2000);
}
