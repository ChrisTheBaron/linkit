#include <GPS_GPRMC.h>

#include <LGPS.h>

gpsSentenceInfoStruct info;

void setup() {

  Serial.begin(9600);

  delay(5000);

  LGPS.powerOn();

  Serial.println("GPS is on");
}

void loop() {
  LGPS.getData(&info);

  String gpsDataRaw = (String)(char*)info.GPRMC;

  GPS_GPRMC dataObj(gpsDataRaw);
  
  Serial.println(
    dataObj.gpsFormat + " " + 
    dataObj.time + " " +
    dataObj.validity + " " +
    dataObj.latitude + " " +
    dataObj.longitude + " " +
    dataObj.speed + " " +
    dataObj.trueCourse + " " +
    dataObj.date + " " +
    dataObj.variation
    );

  delay(2000);
}
